--tests

-- example tests

-- udstart (Node (Op Sum) (Leaf (Val 7)) (Node (Var "R") (Leaf (Var "X")) (Leaf (Val 3)))) (Node (Var "F") (Leaf (Val 7)) (Node (Var "Z") (Leaf (Val 7)) (Leaf (Var "Y"))))
-- udstart (Node (Op Sum) (Node (Op Mul) (Leaf (Var "X")) (Leaf (Val 3))) (Leaf (Val 8))) (Node (Var "F") (Node (Op Mul) (Leaf (Val 7)) (Leaf (Var "Y"))) (Leaf (Val 7)))
-- udstart (Node (Op Sum) (Leaf (Val 7)) (Leaf (Val 9))) (Node (Op Sum) (Node (Op Mul) (Leaf (Var "Y")) (Leaf (Var "X"))) (Leaf (Val 7)))

--new tests

-- udstart (Node (Var "F") (Leaf (Op Sum)) (Node (Val 7) (Leaf (Var "K")) (Leaf (Var "F")))) (Node (Var "Z") (Leaf (Var "T")) (Node (Var "K") (Leaf (Var "Z")) (Leaf (Var "K"))))
-- udstart (Node (Var "F") (Leaf (Op Sum)) (Node (Val 7) (Leaf (Var "K")) (Leaf (Op Mul)))) (Node (Var "Z") (Leaf (Op Sum)) (Node (Var "K") (Leaf (Var "Z")) (Leaf (Var "K"))))




module Main where

import Data.List

data Ope = Sum | Dif | Mul | Div deriving(Show)

instance Eq Ope where
        (==) Sum Sum = True
        (==) Dif Dif = True
        (==) Mul Mul = True
        (==) Div Div = True 
        (==) _ _ = False

data Typ = Double | Ope deriving (Eq)

data Point a b = Pt a b deriving Show

data Nod a b c = Var a | Val b | Op c deriving(Show,Eq)

data Tree a = Leaf a | Node a (Tree a) (Tree a) deriving (Show)

first :: (Bool, [(a,b)],[[String]],[String]) -> Bool
first (b, _ ,_ ,_) = b

second :: (Bool, [(a,b)],[[String]],[String]) -> [(a,b)]
second (_, l,_,_) = l

third :: (Bool, [(a,b)],[[String]],[String]) -> [[String]]
third (_,_,s,_) = s

fourth :: (Bool, [(a,b)],[[String]],[String]) -> [String]
fourth (_,_,_,e) = e

     
checktree :: Tree (Nod String Double Ope) -> Tree (Nod String Double Ope) -> [(String, Nod String Double Ope)] -> [[String]] -> [String] -> (Bool,[(String, Nod String Double Ope)],[[String]],[String])
checktree (Leaf a) (Leaf b) l s e = checknode a b l s e
checktree (Leaf a) (Node t l r) _ _ _= (False, [], [], [])
checktree (Node t l r) (Leaf a) _ _ _= (False, [], [], [])
checktree (Node t1 l1 r1) (Node t2 l2 r2) l s e = let check = checknode t1 t2 l s e
                                                      checkleft = checktree l1 l2 (second check) (third check) (fourth check)
                                                      checkright = checktree r1 r2 (second checkleft) (third checkleft) (fourth checkleft)
                                                in  if first check then ( if first checkleft then checkright else (False,[],[],[])) else (False, [],[],[]) 

checknode :: Nod String Double Ope -> Nod String Double Ope -> [(String, Nod String Double Ope)] -> [[String]] -> [String] -> (Bool,[(String, Nod String Double Ope)],[[String]],[String])
checknode (Op a) (Op b) l s e = (a == b, l, s, e)
checknode (Val a) (Val b) l s e = (a == b, l, s,e)
checknode (Val a) (Op b) l s e = (False, l, s, e)
checknode (Op b) (Val a) l s e = (False, l, s, e)
checknode (Var a) (Var b)  l s e = checkvarvar l (Var a) (Var b) (findkey a l) (findkey b l) s e
checknode (Var a) b l s e = checkvar l a b (findkey a l) s e
checknode b (Var a) l s e = checkvar l a b (findkey a l) s e


findvar :: String -> [String] -> Bool
findvar s [] = False
findvar s (x:xs)
    | s == x = True
    | otherwise = (findvar s xs)
     
findvars  :: String -> [[String]] -> Maybe [String]
findvars v s = find (\x -> findvar v x) s

checkvar :: [(String, Nod String Double Ope)] -> String -> Nod String Double Ope -> Maybe (Nod String Double Ope) -> [[String]] -> [String] -> (Bool,[(String, Nod String Double Ope)],[[String]],[String])
checkvar l v a Nothing s e = let f = findvars v s 
                           in if (f == Nothing) then (True, l ++ [(v,a)], s, e) else (True, bindvars f a, delvars f s, e)
checkvar l v a (Just b) s e
    | a == b = (True, l,s,e)
    | otherwise = (False,l,s,e)
    
bindvars :: Maybe [String] -> Nod String Double Ope -> [(String, Nod String Double Ope)]
bindvars (Just s) a = [(x,a)| x <-s ]

delvars :: Maybe [String] -> [[String]] -> [[String]] 
delvars (Just s) ss = delete s ss
    
checkvarvar :: [(String, Nod String Double Ope)] -> Nod String Double Ope -> Nod String Double Ope -> Maybe (Nod String Double Ope) -> Maybe (Nod String Double Ope) -> [[String]] -> [String] -> (Bool,[(String, Nod String Double Ope)],[[String]],[String])
checkvarvar l (Var a) (Var b) Nothing Nothing s e
   | a == b = (True, l, s, e)
   | otherwise = let iss = issyn a b (findvars a s) (findvars b s) s
                 in if (iss == Nothing) then (False, l, s, e) else (True, l, nojust iss, addeq e a b)
checkvarvar l (Var a) (Var b) (Just c) Nothing s e = bindifsyn l a c b (findvars b s) s e
checkvarvar l (Var b) (Var a) Nothing (Just c) s e = bindifsyn l a c b (findvars b s) s e
checkvarvar l (Var a) (Var b) (Just d) (Just c) s e
    | d == c = (True, l, s, addeq e a b)
    | otherwise = (False, l, s, e)
    
addeq :: [String] -> String -> String -> [String]
addeq e a b 
    | findvar (a ++ " = " ++ b) e = e
    | otherwise = e ++ [a ++ " = " ++ b]
    
bindifsyn :: [(String, Nod String Double Ope)] -> String -> Nod String Double Ope -> String -> Maybe [String] -> [[String]] -> [String] -> (Bool,[(String, Nod String Double Ope)],[[String]],[String])
bindifsyn l a v b Nothing s e = (True, l ++ [(b, v)],s, addeq e a b)
bindifsyn l a v b (Just ss) s e = (True, l ++ (bindvars (Just ss) v), delvars (Just ss) s, addeq e a b)
 
issyn :: String -> String -> Maybe [String] -> Maybe [String] -> [[String]] -> Maybe [[String]]
issyn a b Nothing Nothing s = (Just (s ++ [[a,b]]))
issyn a b (Just sa) Nothing s = (Just ((delete sa s) ++ [sa ++ [b]]))
issyn a b Nothing (Just sb) s = (Just ((delete sb s) ++ [sb ++ [a]]))
issyn a b (Just sa) (Just sb) s
    | sa == sb = (Just s)
    | otherwise = Nothing
    
nojust :: Maybe a -> a
nojust (Just a) = a


findkey :: (Eq k) => k->[(k,v)]->Maybe v
findkey key [] = Nothing
findkey key ((k,v):xs)
    | key == k = Just v
    | otherwise = findkey key xs
    
printvals :: [(String , Nod String Double Ope)]  -> String
printvals [] = ""
printvals ((v,(Val b)) : xs) = " " ++ v ++ " = " ++ (show b) ++ (printvals xs)
printvals ((v,(Op Mul)): xs) = " " ++ v ++ " = " ++ "*" ++ (printvals xs)
printvals ((v,(Op Sum)): xs) = " " ++ v ++ " = " ++ "+" ++ (printvals xs)
printvals ((v,(Op Div)): xs) = " " ++  v ++ " = " ++ "/" ++ (printvals xs)
printvals ((v,(Op Dif)): xs) = " " ++ v ++ " = " ++ "-" ++ (printvals xs)

printvars :: [String] -> String
printvars [] = ""
printvars (s:ss) = " " ++ s ++  (printvars ss)

printres :: (Bool,[(String, Nod String Double Ope)],[[String]],[String]) -> IO ()

printres (True,l,s,e) = do
       print ("Succesfull unification!")
       print (printvals l)
       print (printvars e)
       print ("")
       
printres (False,l,s,e) = do
       print ("")
       print("Error: unification failed")
       print ("")
       
udstart :: Tree (Nod String Double Ope) -> Tree (Nod String Double Ope) -> IO ()
udstart t1 t2 = printres (checktree t1 t2 [] [] [])
                    
       
main :: IO ()
main  =
   do       
      print ("use stack ghci then print udstart tree1 tree2. For example: udstart (Node (Op Sum) (Leaf (Val 7)) (Node (Val 8) (Leaf (Val 7)) (Leaf (Val 3)))) (Node (Op Sum) (Leaf (Val 7)) (Node (Val 8) (Leaf (Val 7)) (Leaf (Val 3)))) ")
 
